resource "aws_security_group" "lpc-test" {
  vpc_id = "${aws_vpc.main.id}"
  name = "application - production"
  description = "security group for my app"
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  } 


  tags {
    Name = "myinstance"
  }
}
