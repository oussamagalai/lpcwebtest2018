angular.module('LpcWebTest2018')
    .service('LpcTranslateService', ['$http', '$rootScope', function ($http, $rootScope) {
        var
            //TODO
            setPropertiesUrlImpl = function (url) {
                $rootScope.changeLocale = function (locale) {
                    $rootScope.locale = locale;
                    localStorage.setItem("locale", $rootScope.locale);
                };
                return $http.get(url);
            },
            loadPropertiesImpl = function () {
                if (!localStorage.getItem("locale"))
                    localStorage.setItem("locale", "en");
                $rootScope.locale = localStorage.getItem("locale");
            }

        return {
            setPropertiesUrl: setPropertiesUrlImpl,
            loadProperties: loadPropertiesImpl
        }
    }]);
