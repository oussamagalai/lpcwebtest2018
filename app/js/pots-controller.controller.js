'use strict';

angular.module('LpcWebTest2018')
    .controller('PotsController', ['$scope', 'PotsService', function ($scope, PotsService) {
        PotsService.get().then(function (res) {
            $scope.pots = res.data;
        })
    }]);
