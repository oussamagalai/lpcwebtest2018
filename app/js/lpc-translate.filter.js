'use strict';

angular.module('LpcWebTest2018')
    .filter('lpcTranslate', ['LpcTranslateService', '$rootScope', function (LpcTranslateService, $rootScope) {
        var translations = undefined;
        var loading = false;
        var filter = function (key, locale) {
            if (loading) {
                return translations[locale][key];
            } else {
                loading = true;
                var promise = LpcTranslateService.setPropertiesUrl('https://recrutement.lepotcommuntest.fr/recruting/2018/get-properties')
                    .then(function (res) {
                        translations = res.data;
                    });
            }
            return promise;
        }
        filter.$stateful = true;
        return filter;
    }]);
