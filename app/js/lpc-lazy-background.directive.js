'use strict';

angular.module('LpcWebTest2018')
    .directive('lpcLazyBackground', [function () {
        var lpcLazyBackgroundLink = function (scope, elem, attr) {
            var img = new Image();
            img.src = scope.lpcLazyBackground;
            img.onload = function () {
                elem.css("background-image", "url(" + scope.lpcLazyBackground + ")");
                elem.css("background-size", "cover");
            };
        }
        return {
            restrict: 'A', scope: {lpcLazyBackground: '='}, link: lpcLazyBackgroundLink
        }

    }]);
