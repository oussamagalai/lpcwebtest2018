# lpcwebtest2018

## Requirments 

Docker must be installed, all the rest is handled with containers.

Have an AWS account, and access/secret keys generated. 

## Create ssh key
`cd infra && ssh-keygen -f mykey`

## Create aws infra 

You need to configure your aws environment. 

`aws configure`  

`docker pull hashicorp/terraform`

`docker run -it -v ${PWD}/infra:/terraform -w="/terraform" hashicorp/terraform init`

`docker run -it -v ${PWD}/infra:/terraform -v $HOME/.aws/credentials:/root/.aws/credentials -w="/terraform" hashicorp/terraform apply`

## Deploy application
`docker pull coxauto/aws-ebcli`

`docker run -it -e "AWS_DEFAULT_REGION=eu-west-1" -e "AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID>" -e "AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>" -w /work -v $PWD:/work coxauto/aws-ebcli eb init`

`docker run -it -e "AWS_DEFAULT_REGION=eu-west-1" -e "AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID>" -e "AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>" -w /work -v $PWD:/work coxauto/aws-ebcli eb deploy`

## Terminate application
`docker run -it -e "AWS_DEFAULT_REGION=eu-west-1" -e "AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID>" -e "AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>" -w /work -v $PWD:/work coxauto/aws-ebcli eb terminate`

## Destroy infra
`docker run -it -v ${PWD}/infra:/terraform -v $HOME/.aws/credentials:/root/.aws/credentials -w="/terraform" hashicorp/terraform destroy`